
# 止めよう!<br>`memset`の乱用

[agate-pris](https://twitter.com/agate_pris)

```note
`memmove`, `memcpy`もまた同様である.
```

---

## 自己紹介

* 中目黒のあるゲーム会社で働いています.
* 趣味でプライベートで<br>ゲーム作ったりしてるマンです.
* その過程で書いたコードを<br>GitHubに投げたりしてました.
* 最近は「でもUnityあるじゃん」に<br>なってしまって絶賛放置中.

```note
もう少し自信がついたらUnity向けに書いたコードも公開したいです.
```

***

## 前書き

---

* 初心者向けの内容です.
* 必ずしも内容の全てが初心者向けな訳ではありません.
* 内容は寧ろ初心者に「こそ」知ってほしい内容です.

```note
この勉強会にいらっしゃる方の多くにとっては当たり前過ぎる内容, 常識レベルの内容かも知れませんがご容赦下さい.
```

---

[C++ MIX #1 - connpass](https://cppmix.connpass.com/event/107576/)

> C++ MIXは発表者に、「すごい発表者による、すごく高度な技術の発表」は求めません。

---

[C++ MIX #1 - connpass](https://cppmix.connpass.com/event/107576/)

> C++ MIXでは「こんな初歩的な発表は歓迎されないだろうな〜」とか悲観的なことは考える必要なく、カジュアルに「こんな話を持ってきたから聞いて聞いて！」という感じで発表してもらいたいです。

---

# 素晴らしい!!

```note
* [C++の勉強会を新しく企画しました - Faith and Brave - C++で遊ぼう](https://faithandbrave.hateblo.jp/entry/2018/11/05/141543)

> Boost.勉強会は ... もともと ... 「C++周辺の勉強会」である

知らなかった……
```

---

* C++に興味を持った初学者に「難しすぎて何も分からなかった」という気持ちで帰ってほしくない, 何か持ち帰って欲しい.
* この勉強会が初心者から上級者まで, 幅広い層に支持されるものになって欲しい.
* そのために自分から積極的に発信していく.

---

* 独習C++とEffective C++やModern Effective C++の間に知るべき「まとまったノウハウ」という物は意外と多くない.
* 世の中から残念なコードを少しでも減らしたい.
* ~~自分自身が残念なコードによって苦しめられたくない.~~

***

## memsetとは

```cpp
namespace std {
    void* memcpy(void* s1, const void* s2, size_t n);
    void* memmove(void* s1, const void* s2, size_t n);
    void* memset(void* s, int c, size_t n);
}
```

* `memcpy` と `memmove` もついでに記載.
* `s` で指定された領域に `c` をコピーする.
* `n` で初期化する領域のバイト長を指定する.

---

## 初心者にありがちな過ち

* オブジェクトの初期化でデータメンバを初期化し忘れると値が不定になる.
* そのまま読み取るとUndefined Behaviour.
* じゃあ `memset` で初期化すればいいじゃん!

```note
C++初心者だけでなくCに慣れた開発者にもありがちな過ちです.
```

---

## Bad object initialization code

```cpp
struct s {
    int i;
    long l;
    float f;
    double d;
};

void f(s& a) {
    std::memset(&a, 0, sizeof(a));
}
```

```note
マクロに置き換えたり配列に対応したコードをしばしば見かけますが, 標準規格を把握した上でクリティカルなケースで適用していない限りより状況を悪化させていると言えます.

命名がひどいのはスライド上で短く表示するためです, ご容赦下さい.
```

---

# ダメです!!

---

## 何がだめなのか

* データ型の内部表現は処理系定義
* 対象のデータ次第ではその動作は未定義

***

### データ型の内部表現は処理系定義

```cpp
int i = 1;
std::memset(&i, 0, sizeof(i));
assert(i == 0);
```

C++では整数型の内部表現は「pure binary numeration system」によって表現される, という以上の規程がほぼ無いため, `0` 初期化された領域の値が必ずしも `0` とは限らない.

```note
これは整数型に限った話ではなく, 様々な型についてその内部表現を正確に把握するためには細かな知識が必要となり, また, 内部表現を把握しなければならないコードは可読性や可搬性を残っている可能性が極めて高く, そのようなコードは一般に避けるべきです.

細かい内容は規格のバージョン毎に異なりますが, そもそも規格のバージョンに依存するコードを書いている時点でそのコードは可搬性の低い, 行儀の悪いコードであると言えます.

実行速度と可読性は必ずしも相反するものではありませんが, (経験則として) 可搬性と可読性には相関があると私は考えています.
```

---

#### [N4659](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/n4659.pdf) 6.9.1 Fundamental types [basic.fundamental]

この項目に基本型に関する要件が書かれています. 基本型は新しい規格程要件の定義が厳しくなっていく傾向にあります. 一部はCの標準を参照しています.

---

> 7. ... The representations of integral types shall define values by use of a pure binary numeration system.

---

> 52) A positional representation for integers that uses the binary digits 0 and 1, in which the values represented by successive bits are additive ... (Adapted from the American National Dictionary for Information Processing Systems.)

***

自分が書いているコードがターゲットとする全ての処理系における整数型, 浮動小数点型, その他の型の内部的表現を把握している者だけが `memset` によって初期化してもよい.

```note
まぁ, 冗談ですが.

誇張はありますが, 要するに不用意な `memset` はバグの原因となるという事です.
```

---

自分が書いているコードが将来的に恒久的にターゲットとしうる全ての処理系を把握している者だけが `memset` によって初期化してもよい.

```note
前ページに引き続き, 同様.
```

---

### 対象のデータ次第ではその動作は未定義

```cpp
struct foo { int i; };
class bar { virtual void f(); };
void f(foo& f, bar& b) {
    std::memset(&f, 0, sizeof(f));  // defined
    std::memset(&b, 0, sizeof(b));  // not defined
}
```

`foo` の初期化においても `i` の「値」が `0` になる訳ではなく, あくまでそのメモリ領域が `0` 初期化されるに過ぎない事に注意. 後者の場合そもそも正しく領域が初期化されない可能性がある.

```note
未定義であるという事は (ざっくり言えば) コンパイラは如何なるコードをも出力し得るという事です. あなたのプログラムを突然終了してもいいし, 突然コンピュータをハングさせてもよい, ということです.
```

---

* トリビアルコピー可能な型のみ `memcpy` によって正しくコピー可能な事が保証されている.
* 要件は決して単純ではなく, 特に複数人が携わるプロジェクトともなれば容易にメモリ破壊を引き起こしうる.
* あなた自身, あるいは他の誰かの改修によってプログラムが壊れない事を保証するのが極めて困難.

```note
少し前の規格ではもう少し厳しい制限になっています.

* [P0767R0: Expunge POD](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/p0767r0.html)

規格上の制限の緩和にともない上記の提案が為されています.
```

---

### 6.9 Types [basic.types]

> 2 For any object ... of trivially copyable type T, ... the underlying bytes (4.4) making up the object can be copied into an array of char, unsigned char, or std::byte (21.2.1). If the content of that array is copied back into the object, the object shall subsequently hold its original value.

---

### 6.9 Types [basic.types]

> 3 For any trivially copyable type T, if two pointers to T point to distinct T objects obj1 and obj2, ... if the underlying bytes (4.4) making up obj1 are copied into obj2, obj2 shall subsequently hold the same value as obj1.

---

### 12 Classes [class]

> 6 A trivially copyable class is a class:
> (6.1) where each copy constructor, move constructor, copy assignment operator, and move assignment operator (15.8, 16.5.3) is either deleted or trivial,
> (6.2) that has at least one non-deleted copy constructor, move constructor, copy assignment operator, or move assignment operator, and
> (6.3) that has a trivial, non-deleted destructor (15.4).
> A trivial class is a class that is trivially copyable and has one or more default constructors (15.1), all of which are either trivial or deleted and at least one of which is not deleted. [Note: In particular, a trivially copyable or trivial class does not have virtual functions or virtual base classes. — end note]

```note
滅茶滅茶はみ出てますが, それだけ込み入っている, という事です. 気になる方は直接規格を参照して下さい.
```

---

もしもあなたのコードに `memset` が多用されている場合, それはその全てを把握しなければ**安全なコードを書くことが出来ない**事を意味します.

---

更には将来的にその型に変更を加えたくなった時, トリビアルコピー可能でない型への変更を伴う変更が実質的に不可能になる事を意味し, **柔軟性, 保守性を大きく損なう**事を意味します.

---

ここから導き出せる結論は「**余程**の理由がない限り, オブジェクトの初期化に `memset`を使ってはならない」という事です.

---

## 「でも, 速いんでしょ?」

---

## あなたのプログラムのボトルネックは本当にそこですか?

---

## 早期最適化ではありませんか?

---

# そもそもきちんとプロファイラで実行時間を計測しましたか?

---

可読性と実行速度は
必ずしも相反する訳ではないが,
通常「まず可読性と可搬性. 最適化は最後」.
可読性, 可搬性, 実行速度の比重は
プロダクト次第.

---

## ロブ・パイクのプログラミングの5つのルール

* [Rob Pike: Notes on Programming in C](http://www.lysator.liu.se/c/pikestyle.html)
* [Rob Pike's 5 Rules of Programming]( http://users.ece.utexas.edu/~adnan/pike.html)
* [ロブ・パイク - Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%AD%E3%83%96%E3%83%BB%E3%83%91%E3%82%A4%E3%82%AF)
* [UNIX哲学 - Wikipedia](https://ja.wikipedia.org/wiki/UNIX%E5%93%B2%E5%AD%A6)

---

> ルール1: プログラムがどこで時間を消費することになるか知ることはできない。ボトルネックは驚くべき箇所で起こるものである。したがって、どこがボトルネックなのかをはっきりさせるまでは、推測を行ったり、スピードハックをしてはならない。

---

> ルール2: 計測すべし。計測するまでは速度のための調整をしてはならない。コードの一部が残りを圧倒しないのであれば、なおさらである。

---

> ルール4: 凝ったアルゴリズムはシンプルなそれよりバグを含みやすく、実装するのも難しい。シンプルなアルゴリズムとシンプルなデータ構造を使うべし。

```note
ルール3と5, 6はここでは割愛します.
```

---

## ではどうオブジェクトを初期化するか?

---

* クラスの初期化でデータメンバが多すぎて初期化を忘れる場合, そもそも忘れてしまう程データメンバが多い**設計自体が破綻している**可能性が高い.
* 集成体は集成体初期化を行うべし.

---

* 集成体は集成体初期化を使う事で安全に初期化出来る.
* 集成体でない型, かつトリビアルコピー可能な型であっても, `memset` や `memcpy` を不用意に用いてよい理由にはならない.

---

### 集成体はユーザ定義のコンストラクタを持たない. ユーザ定義のコンストラクタを持つ型であれば, `memset` による初期化はその型の設計思想に反するので一般に避けるべきである.

---

### 集成体は `private` または `protected` な非静的なデータメンバを持たない. それらのデータメンバを持つ型を `memset` によって初期化する事はアクセス指定子によるカプセル化を破壊する行為であり, その型の設計思想に反するので一般に避けるべきである.

---

### 集成体は仮想関数を持たない. 仮想関数を持つ型はトリビアルコピー不可能な型になるため, `memset` や `memcpy` を使用してはならない.

---

### 集成体は `virtual` な基底クラスを持たない. `virtual` 基底クラスを持つ型は非トリビアルコピー不可能な型であるため. `memset` や `memcpy` を適用してはならない.

---

### 集成体は `private` または `protected` な基底クラスを持たない. `private` や `protected` な基底クラスを `memset` や `memcpy` によって変更する行為はカプセル化を破壊する行為で一般的に避けるべきである. 更にはそれは「基底クラスの設計が派生クラスに対する処理に依存している状況」であり, 一般的に避けるべきである.

```note
基底クラスの設計者が派生クラスに対する処理等知る由もないため, 深刻な問題を引き起こしうる.
```

---

## 以上より「集成体は集成体初期化を行うべき」であり, 「集成体以外に `memset` を適用するのは避けるべき」である.

---

## 「でも, 速いんでしょ?」 (2回目)

---

# 速くない

* 条件付き.
* ボトルネックは本当にそこですか?
* 早期最適化では?
* プロファイラで測りましたか?
* アセンブリを読みましたか?
* そもそもそこまでして速くしたいなら初期化しないのが速い.

---

## [`init_obj_agr.cpp`](init_obj_agr.cpp)

```cpp
#include <iostream>
struct s { int i; long l; float f; double d; };
void print(s const& a) {
    std::cout
        << a.i << '\n' << a.l << '\n'
        << a.f << '\n' << a.d << std::endl;
}
int main() {
    auto const a = s{};
    print(a);
}
```

```note
ファイル名がよくない, という話はここでは置いておいて下さい.
```

---

## [`init_obj_mem.cpp`](init_obj_mem.cpp)

```cpp
#include <cstring>
#include <iostream>
struct s { int i; long l; float f; double d; };
void print(s const& a) {
    std::cout
        << a.i << '\n' << a.l << '\n'
        << a.f << '\n' << a.d << std::endl;
}
int main() {
    s a;
    std::memset(&a, 0, sizeof(s));
    print(a);
}
```

---

```
$ clang++ --version
clang version 7.0.0 (tags/RELEASE_700/final)
Target: x86_64-w64-windows-gnu
Thread model: posix
InstalledDir: C:\tools\msys64\mingw64\bin
$ clang++ init_obj_agr.cpp -O -S
$ clang++ init_obj_mem.cpp -O -S
```

# 全く同じアセンブリを出力する

* [`init_obj_agr.s`](init_obj_agr.s)
* [`init_obj_mem.s`](init_obj_mem.s)

```note
ファイル名に関する埋め込みは除く.
```

---

## 配列なら?

---

## [`init_ary_agr.cpp`](init_ary_agr.cpp)

```cpp
#include <iostream>
struct s { int i; long l; float f; double d; };
void print(s const& a) {
    std::cout
        << a.i << '\n' << a.l << '\n'
        << a.f << '\n' << a.d << std::endl;
}
int main() {
    s const a[16]{};
    for (auto const& e : a) {
        print(e);
    }
}
```

```note
出力されるアセンブリコードの単純化のため, ここでは `std::array` を使っていません.

配列のサイズを初期化子によって指定したい場合以外は `std::array` を使うべきです.

配列のサイズを測る時はマクロを使わず `boost::size` を使いましょう. コンパイル時にサイズを測りたい時は `constexpr` とテンプレートを使いましょう (Boostにコンパイル時に配列のサイズをマクロを使わずに取得出来る関数があったら教えて下さい).
```

---

## [`init_ary_mem.cpp`](init_ary_mem.cpp)

```cpp
#include <cstring>
#include <iostream>
struct s { int i; long l; float f; double d; };
void print(s const& a) {
    std::cout
        << a.i << '\n' << a.l << '\n'
        << a.f << '\n' << a.d << std::endl;
}
int main() {
    s a[16];
    std::memset(a, 0, sizeof(s) * 16);
    for (auto const& e : a) {
        print(e);
    }
}
```

```note
態々 `sizeof` に対して `a` の代わりに `s` を渡しているのは変数を `sizeof` 演算子に渡した時にコンパイル時に解決されるのか, という質問を無用にするためです.
```

---

```
$ clang++ --version
clang version 7.0.0 (tags/RELEASE_700/final)
Target: x86_64-w64-windows-gnu
Thread model: posix
InstalledDir: C:\tools\msys64\mingw64\bin
$ clang++ init_ary_agr.cpp -O -S
$ clang++ init_ary_mem.cpp -O -S
```

# 全く同じアセンブリを出力する

* [`init_ary_agr.s`](init_ary_agr.s)
* [`init_ary_mem.s`](init_ary_mem.s)

---

* 最適化はプロファイラでボトルネックを特定してから行うのが基本.
* 規格とコンパイラを正しく理解しないと誤った最適化を施してしまう.
* コンパイラは想像する以上に頭が良い.

```note
時にコンパイラは想像以上に頭が悪い場合もありますがご容赦下さい.

規格に対して提案を行えるレベルの卓越したプログラマや, コンパイラの実装に携わるレベルの卓越したプログラマ, その他卓越したプログラマであれば話は変わってきますが.
```

---

## Cの場合

```c
typedef struct s { int i; long l; float f; double d; } s_typedef;
int main() {
    struct s a = {};
    s_typedef b = {};
    struct s c[16] = {};
    s_typedef d[16] = {};
}
```

例えCであっても `memset` を用いて初期化してよい理由にはならない.

---

## 集成体初期化と代入

```cpp
#include <array>
struct s { int i; long l; float f; double d; };
void f() {
    auto s_obj = s{};
    s s_array[16] = {};
    auto std_array_s = std::array<s, 16>{};
    s_obj = {};
    for (auto& e : s_array) e = {};
    std_array_s = {};
}
```

`std::array` は生配列と比較してパフォーマンスを損なわず, 再代入もより簡潔に書ける.

C++では `auto` を優先的に使う.

```note
`const` を使える時は積極的に `const` を使いましょう.

「このコードは意味がないのでコード自体が消去される」という指摘はご容赦下さい.

生配列への代入による初期化の方法は基本的に無い (あったら教えて下さい).
```

---

## ではいつ `memset` や `memcpy` を使うのか

---

* バイト列の移動.
* バイト列のコピー.
* バイト列の初期化.
* C++でのバイト列とは `char` の配列を指す.

---

特に文字列の処理で使用する事を想定しているのは定義されているヘッダが `<cstring>` である事からも明白である.

(ただしC++では文字列は `std::string` や `std::stringstream` を使いましょう)

```note
標準ライブラリで **どうしても** 機能的に足りない場合はオレオレライブラリを作ってもいいですが, 標準ライブラリの実装者がどういった意図でそのような設計にしたのかをよく考えましょう.
```

---

その他はコンテナの実装, メモリ管理系の実装, メモリバンク関連の実装等.

つまりは, 一般的なアプリケーションで `memset` や `memcpy` `memmove` の様な破壊的な行為はしないし, すべきではない.

---

`boost::optional` や `boost::container::static_vector` の様なplacement-newを行うことでコンストラクタ/デストラクタの明示的な操作とメモリ割り当てを行うようなライブラリの多くは内部的に割当を行う領域を `char` (または `unsigned char`) の配列で持っている.

---

## コンストラクタでの集成体の初期化

```cpp
struct s { int i; long l; float f; double d; }
class c {
    private:
    s m_s;
    public:
    c() : m_s() {}
};
```

この様に初期化する.
値をセットする場合は波括弧を用いれば良い.

```note
上記ではデフォルトコンストラクタで各メンバをデフォルトの値で初期化している.

デフォルトコンストラクタでデフォルト値以外でメンバを初期化するのは直感に反する設計である可能性が高いため, 十分に注意が必要である.
```

---

## メンバ自身のデフォルトコンストラクタでの初期化

デフォルトコンストラクタで値を初期化するために `boost::value_initialized` を使うという手もあります (ただしアクセスが冗長になります).

---

## リンク・参考文献 1

* [C++ MIX #1 - connpass](https://cppmix.connpass.com/event/107576/)
* [C++の勉強会を新しく企画しました - Faith and Brave - C++で遊ぼう](https://faithandbrave.hateblo.jp/entry/2018/11/05/141543)
* [C++国際標準規格 - cpprefjp C++日本語リファレンス](https://cpprefjp.github.io/international-standard.html)

---

## リンク・参考文献 2

* [N4659 Working Draft, Standard for Programming Language C++](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/n4659.pdf)
* [P0767R0: Expunge POD](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/p0767r0.html)
* [std::memset - cppreference.com](https://ja.cppreference.com/w/cpp/string/byte/memset)

```note
どの規格を参照すればいいのか, どういった基準で公開・非公開を決められているのかよく分かりませんでしたが, 今回は既にDISが出ている最も新しい規格であるC++17の, ウェブ上で無料で入手できる最も新しい文書であるN4659を参照しました.

DISは委員会メンバー限定? ISOからPDF買おうと思ってもスイスフランでと言われると躊躇する...
```

---

## リンク・参考文献 3

* [Boost Value Initialized Library Documentation](https://www.boost.org/doc/libs/release/libs/utility/value_init.htm)

***

## 余談

---

このスライドは`reveal-ck`を使って作っています.

---

不慣れなのでトラブル続きでとても大変でした.

---

* 文字コードで激しくエラーが発生した.
* 解決方法が分からず仮想環境をインストールした.
* 環境はVirtual Box + Vagrant + Amazon Linux AMI

---

* 仮想環境で上手く動いたが,
  共有フォルダ上のファイルを
  Windows から編集した時,
  rebuildとreloadが上手く走らなかった.
* この点については `rsync` を使うと上手く動くと聞いたが未検証.
* 更にブラウザのreloadを自動で走らせようと思うと
  たぶん仮想環境上でブラウザを動かし
  それをX Window Server等で表示する必要あり.

---

* 最終的には環境変数の設定で
  Windowsで普通に動作した.
* Rubyは2.0からUTF-8がデフォルトだった筈では……？

---

* 更に `reveal-ck` のバグっぽい挙動を引いた (現在 issue を投げてやりとり中)

---

* `config.yml` が空だと関数 `each_pair` が見つからないというエラーが発生.
  原因が分かりづらかった.

***

## 質疑・応答
