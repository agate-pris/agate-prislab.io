	.text
	.def	 __dtor__ZStL8__ioinit;
	.scl	3;
	.type	32;
	.endef
	.p2align	4, 0x90         # -- Begin function __dtor__ZStL8__ioinit
__dtor__ZStL8__ioinit:                  # @__dtor__ZStL8__ioinit
# %bb.0:
	leaq	_ZStL8__ioinit(%rip), %rcx
	jmp	_ZNSt8ios_base4InitD1Ev # TAILCALL
                                        # -- End function
	.def	 _Z5printRK1s;
	.scl	2;
	.type	32;
	.endef
	.globl	_Z5printRK1s            # -- Begin function _Z5printRK1s
	.p2align	4, 0x90
_Z5printRK1s:                           # @_Z5printRK1s
.seh_proc _Z5printRK1s
# %bb.0:
	pushq	%rsi
	.seh_pushreg 6
	pushq	%rdi
	.seh_pushreg 7
	subq	$40, %rsp
	.seh_stackalloc 40
	.seh_endprologue
	movq	%rcx, %rsi
	movl	(%rcx), %edx
	leaq	_ZSt4cout(%rip), %rcx
	callq	_ZNSolsEi
	movb	$10, 37(%rsp)
	leaq	37(%rsp), %rdx
	movl	$1, %r8d
	movq	%rax, %rcx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	movl	4(%rsi), %edx
	movq	%rax, %rcx
	callq	_ZNSo9_M_insertIlEERSoT_
	movb	$10, 38(%rsp)
	leaq	38(%rsp), %rdx
	movl	$1, %r8d
	movq	%rax, %rcx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm1
	movq	%rax, %rcx
	callq	_ZNSo9_M_insertIdEERSoT_
	movb	$10, 39(%rsp)
	leaq	39(%rsp), %rdx
	movl	$1, %r8d
	movq	%rax, %rcx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	movsd	16(%rsi), %xmm1         # xmm1 = mem[0],zero
	movq	%rax, %rcx
	callq	_ZNSo9_M_insertIdEERSoT_
	movq	%rax, %rsi
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%rsi,%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB1_5
# %bb.1:
	cmpb	$0, 56(%rdi)
	je	.LBB1_3
# %bb.2:
	movb	67(%rdi), %dl
	jmp	.LBB1_4
.LBB1_3:
	movq	%rdi, %rcx
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rdi), %rax
	movb	$10, %dl
	movq	%rdi, %rcx
	callq	*48(%rax)
	movl	%eax, %edx
.LBB1_4:
	movq	%rsi, %rcx
	callq	_ZNSo3putEc
	movq	%rax, %rcx
	callq	_ZNSo5flushEv
	nop
	addq	$40, %rsp
	popq	%rdi
	popq	%rsi
	retq
.LBB1_5:
	callq	_ZSt16__throw_bad_castv
	ud2
	.seh_handlerdata
	.text
	.seh_endproc
                                        # -- End function
	.def	 main;
	.scl	2;
	.type	32;
	.endef
	.globl	main                    # -- Begin function main
	.p2align	4, 0x90
main:                                   # @main
.seh_proc main
# %bb.0:
	pushq	%rbp
	.seh_pushreg 5
	pushq	%rsi
	.seh_pushreg 6
	subq	$424, %rsp              # imm = 0x1A8
	.seh_stackalloc 424
	leaq	128(%rsp), %rbp
	.seh_setframe 5, 128
	.seh_endprologue
	callq	__main
	leaq	-96(%rbp), %rsi
	xorl	%edx, %edx
	movl	$384, %r8d              # imm = 0x180
	movq	%rsi, %rcx
	callq	memset
	movq	%rsi, %rcx
	callq	_Z5printRK1s
	leaq	-72(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	-48(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	-24(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	24(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	48(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	72(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	96(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	120(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	144(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	168(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	192(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	216(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	240(%rbp), %rcx
	callq	_Z5printRK1s
	leaq	264(%rbp), %rcx
	callq	_Z5printRK1s
	xorl	%eax, %eax
	addq	$424, %rsp              # imm = 0x1A8
	popq	%rsi
	popq	%rbp
	retq
	.seh_handlerdata
	.text
	.seh_endproc
                                        # -- End function
	.def	 _GLOBAL__sub_I_init_ary_mem.cpp;
	.scl	3;
	.type	32;
	.endef
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_init_ary_mem.cpp
_GLOBAL__sub_I_init_ary_mem.cpp:        # @_GLOBAL__sub_I_init_ary_mem.cpp
.seh_proc _GLOBAL__sub_I_init_ary_mem.cpp
# %bb.0:
	subq	$40, %rsp
	.seh_stackalloc 40
	.seh_endprologue
	leaq	_ZStL8__ioinit(%rip), %rcx
	callq	_ZNSt8ios_base4InitC1Ev
	leaq	__dtor__ZStL8__ioinit(%rip), %rcx
	addq	$40, %rsp
	jmp	atexit                  # TAILCALL
	.seh_handlerdata
	.text
	.seh_endproc
                                        # -- End function
	.lcomm	_ZStL8__ioinit,1        # @_ZStL8__ioinit
	.section	.ctors,"dw"
	.p2align	3
	.quad	_GLOBAL__sub_I_init_ary_mem.cpp

