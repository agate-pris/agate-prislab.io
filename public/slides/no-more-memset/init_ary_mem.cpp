#include <cstring>
#include <iostream>
struct s { int i; long l; float f; double d; };
void print(s const& a) {
    std::cout
        << a.i << '\n' << a.l << '\n'
        << a.f << '\n' << a.d << std::endl;
}
int main() {
    s a[16];
    std::memset(a, 0, sizeof(s) * 16);
    for (auto const& e : a) {
        print(e);
    }
}
